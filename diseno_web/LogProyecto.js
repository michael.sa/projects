
var rideGuardado = JSON.parse(localStorage.getItem("rides"));

/*
function pata registrar nuevos usuarios
*/
function registrarUser() {
	var nombre = document.getElementById('nombre').value;
	var apellido = document.getElementById('apellido').value;
	var	 telefono = document.getElementById('tele').value;
	var usuario = document.getElementById('username').value;
	var password = document.getElementById('password').value;
	var passwordDos = document.getElementById('pass').value;
	if (nombre == "" || apellido == "" || telefono == "" || usuario == "" 
		|| password == "" || passwordDos == "") {
		alert("Por favor complete todo el formulario");
	} else {
		var newUser = JSON.parse(localStorage.getItem("user"));
	debugger;
	var usuarioN = [];
	for (var i = 0; i < newUser.length; i++) {
		usuarioN[i] = newUser[i].usuario;

	}
		if(usuarioN.indexOf(usuario) > -1){
			alert("Lo sentimos nombre usuario no disponible");
			
		} else if (password != passwordDos) {
			alert("Los passwords no coinciden");
		} else {
			
			var persona = {
			nombre: nombre,
			apellido: apellido,
			telefono: telefono,
			usuario: usuario,
			password: password,
			};	

	
	if (newUser == null) {
		newUser = [];
	}
		newUser.push(persona);
		localStorage.setItem("user", JSON.stringify(newUser));
	alert("Usuario Guardado con exito!!");
	window.open("Login.html","_self");
		}
	}
}


/*
Evento y function para login
*/
if(document.getElementById('login')) {
    document.getElementById('login').addEventListener('click', function() {
  		document.getElementById('mensaje').innerHTML = "";
		var usuarios = JSON.parse(localStorage.getItem("user"));
		var user = document.getElementById('usern').value;
		var pass = document.getElementById('pass').value;
		if (user == "" || pass == "") {
			alert("Favor de ingresar todos los datos");
		} else {
			if (usuarios == null){
			alert("Usuario no encontrado");
		} else {
			
			for (var i = 0; i < usuarios.length; i++) {
			if (usuarios[i].usuario == user && usuarios[i].password == pass) {
			
			localStorage.setItem("usuarioLog", usuarios[i].usuario);
			document.getElementById('mensaje').innerHTML = "";
			window.open("Dashboard.html","_self");
			return;
		}

		}
		document.getElementById('mensaje').innerHTML = "Usuario o Password incorrecto";
		}
		}
     });
}


/*
Funtion para leer cual usuario esta logueado 
*/
function leerLog(){
	var log = localStorage.getItem("usuarioLog");
	
	document.getElementById('user').innerHTML = "Welcome " + log;
	return log;
}


/*
Function para guardar rides 
*/
function guardarRides() {
	var usuario = leerLog();
	var rideName = document.getElementById('rideName').value;
	var start = document.getElementById('start').value;
	var end = document.getElementById('end').value;
	var descripcion = document.getElementById('description').value;
	var departure = document.getElementById('departure').value;
	var arrival = document.getElementById('arrival').value;
	var dias = [];


	for (var i = 0; i < 7; i++) {
		if(document.forms["dias"].elements[i].checked){
			dias.push(document.forms["dias"].elements[i].value);
			
		} else {
			dias.push("");
		} 
	}
		var ride = {
		usuario: usuario,
		rideName: rideName,
		start: start,
		end: end,
		descripcion: descripcion,
		departure: departure,
		arrival: arrival,
		dias: dias
	};
	var newRide = JSON.parse(localStorage.getItem("rides"));
	if (newRide == null) {
		newRide = [];
	}
		newRide.push(ride);
		localStorage.setItem("rides", JSON.stringify(newRide));
	alert("Ride Guardado con exito!!");
	window.open("Dashboard.html","_self");
	
}

/*
Function para leer los rides de un usuario
*/
function leerRides(){
	
	if (rideGuardado == null){
		alert("Ride no encontrado");
	} else {
		var cont = 1;
		for (var i = 0; i < rideGuardado.length; i++) {
		if (rideGuardado[i].usuario == leerLog()) {
			
			
			var row = "<tr id=\"" +i+"\"><td>"+rideGuardado[i].rideName+"</td><td>"+rideGuardado[i].start+"</td><td>"
			+rideGuardado[i].end+"</td><td><input type=\"button\" name=\"name\" " +
			"value = \"Edit\" class=\"btn btn-info\" onclick=\"ride_Modificar("+cont+","+1+");\"><span>--</span><input type=\"button\" "+
			" name=\"name\" value=\"Delete\" class=\"btn btn-danger\"onclick=\"ride_Modificar("+cont+","+2+");\"></td></tr>";
			
			// agregar a la tabla
			var table = document.getElementById("rides_table");
			table.innerHTML = table.innerHTML + row;
			cont++;
		} 
		
		}
	}	
	
}

/*
Function para guardar en localstorage un ride que se va a modificar
o eliminar
*/
function ride_Modificar(index,tipo){
	var user = leerLog();
	var start = document.getElementById('rides_table').tBodies[index].rows[0].cells[1].innerHTML;
	var end = document.getElementById('rides_table').tBodies[index].rows[0].cells[2].innerHTML;

	var rideModifiar ={
		user: user,
		start: start,
		end: end,
	};

		localStorage.setItem("ride-modificar", JSON.stringify(rideModifiar));
		
		if(tipo==1){
			
			window.open("Rides.html","_self");
		} else {
			eliminarRide();
		}
	
}

/*
Function para editar datos de un ride
*/
function editarRides(){
	var rideEditar = JSON.parse(localStorage.getItem("ride-modificar"));
	for (var i = 0; i < rideGuardado.length; i++) {
		if (rideGuardado[i].usuario == rideEditar.user && rideGuardado[i].start == rideEditar.start 
			&& rideGuardado[i].end == rideEditar.end) {
			
			document.getElementById('rideName').value = rideGuardado[i].rideName;
			document.getElementById('start').value = rideGuardado[i].start;
			document.getElementById('end').value = rideGuardado[i].end;
			document.getElementById('description').innerHTML = rideGuardado[i].descripcion;
			var dia = rideGuardado[i].dias;
			for (var j = 0; j < 7; j++) {
			if (document.forms["dias"].elements[j].value == dia[j]) {
					document.getElementById(document.forms["dias"].elements[j].value).checked = true;
					
				}
		
			}
			
		return;
		}
			
	}
}


/*
Function para eliminar un ride
*/
function eliminarRide(){
	var rideEditar = JSON.parse(localStorage.getItem("ride-modificar"));
	if (confirm("Desea eliminer o modificar este Ride")) {
        
		for (var i = 0; i < rideGuardado.length; i++) {
			if (rideGuardado[i].usuario == rideEditar.user && rideGuardado[i].start == rideEditar.start && 
				rideGuardado[i].end == rideEditar.end) {
				rideGuardado.splice(rideGuardado.indexOf(rideGuardado[i]),1);
				localStorage.setItem("rides", JSON.stringify(rideGuardado));
				return;
			}
		}
    }
}


/*
Evento y function para buscar rides en la paguina de inicio
*/
if(document.getElementById('buscar_rides')) {
    document.getElementById('buscar_rides').addEventListener('click', function() {
		var from = document.getElementById('start').value;
		var to = document.getElementById('end').value;
		var table = document.getElementById("results");
				table.innerHTML = table.innerHTML = "";
		if (rideGuardado == null){
			alert("Ride no encontrado");
		} else {
			var cont = 0;
			for (var i = 0; i < rideGuardado.length; i++) {
				if (rideGuardado[i].start.toLowerCase() == from.toLowerCase() && 
					rideGuardado[i].end.toLowerCase() == to.toLowerCase()) {
				cont = i;
				
				var row = "<tr><td>"+rideGuardado[i].usuario+"</td><td>"+rideGuardado[i].start+"</td><td>"
				+rideGuardado[i].end+"</td><td><input type=\"button\" "+
				" name=\"name\" value=\"View\" class=\"btn btn-info\"onclick=\"rideView("+cont+");\"></td></tr>";

				// agregar a la tabla
				table.innerHTML = table.innerHTML + row;

			}
			}	
		}
    });
}

/*
Function para guardar en localstorage un ride del cual se mostraran los datos a un usario
*/
function rideView(index){

	localStorage.setItem('ride-mostrar',index);
	window.open("ViewRides.html","_self");
}

/*
Function para cargar los datos de un ride que se esta mostrando a un usuario
*/
function vistaRide(){
	
	var settings = JSON.parse(localStorage.getItem("settings"));
	var index = localStorage.getItem('ride-mostrar');
	document.getElementById('rideName').innerHTML = rideGuardado[index].rideName;
	document.getElementById('start').innerHTML = rideGuardado[index].start;
	document.getElementById('end').innerHTML = rideGuardado[index].end;
	document.getElementById('description').innerHTML = rideGuardado[index].descripcion;
	document.getElementById('departure').innerHTML = rideGuardado[index].departure;
	document.getElementById('arrival').innerHTML = rideGuardado[index].arrival;
	document.getElementById('dias-rides').innerHTML = rideGuardado[index].dias;
	var dia = rideGuardado[index].dias;
	var dias = "";
	for (var j = 0; j < dia.length; j++) {
		if (dia[j] != "") {
	      dias += dia[j] + ","; 
		}
		document.getElementById('dias-rides').innerHTML = dias;
		
	}
	
	var datos = "";
	for (var i = 0; i < settings.length; i++) {
		if(settings[i].usuario == rideGuardado[index].usuario){
		datos+= "Usuario: " +settings[i].usuario +",\r"+ 
		 "Nombre: "+ settings[i].nombre + ",\r"+
		 "Speed: " + settings[i].speed +",\r"+ 
		 "Datos: " + settings[i].datos;	
		}
	}
	document.getElementById('datos_creador').innerHTML = datos;

}

/*
Function para guardar settings de un usario
*/
function guardarSettings(){
	var usuario = leerLog();
	var nombre = document.getElementById('fullname').value;
	var speed = document.getElementById('speed').value;
	var datos = document.getElementById('datos_user').value;
	var persona = {
		usuario: usuario,
		nombre: nombre,
		speed: speed,
		datos: datos,
	};	

	var newSetting = JSON.parse(localStorage.getItem("settings"));
	if (newSetting == null) {
		newSetting = [];
	}
		newSetting.push(persona);
		localStorage.setItem("settings", JSON.stringify(newSetting));
		alert("Setting con exito!!");
		window.open("Dashboard.html","_self");
	}

/*
Function para leer los settigs de un usuario
*/
function leerSettings(){
	var settingGuardado = JSON.parse(localStorage.getItem("settings"));
	var usuario = leerLog();
		
		for (var i = 0; i < settingGuardado.length; i++) {
		if (settingGuardado[i].usuario == usuario) {
			document.getElementById('datos_creador').innerHTML = "Nombre:" +settingGuardado[i].nombre + 
			"Velocidad promedio:" + settingGuardado[i].speed +
			"Datos de la persona:"+ settingGuardado[i].datos; 
			return;
		} 

		
		}
		
}

/*
Function para editar los settings de un usario
*/
function editarSettigns(){
	var settingGuardado = JSON.parse(localStorage.getItem("settings"));
	for (var i = 0; i < settingGuardado.length; i++) {
			if (settingGuardado[i].usuario == leerLog()) {
				document.getElementById('fullname').value = settingGuardado[i].nombre;
				document.getElementById('speed').value = settingGuardado[i].speed;
				document.getElementById('datos_user').value = settingGuardado[i].datos;
			}

			document.getElementById('editar_setting').addEventListener("click", function(){
				settingGuardado.splice(settingGuardado.indexOf(settingGuardado[i]),1);
				localStorage.setItem("settings", JSON.stringify(settingGuardado));
				guardarSettings();
			});	
		}
}


