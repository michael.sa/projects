<?php  
/**
* 
*/
class Principal_model extends CI_Model
{
	
	function __construct()
	{
		parent::__construct();
	}

	//Para buscar un ride
	public function BuscarRides($start,$end)
	{
		$query = $this->db->get_where('ride',
      	array('start' => $start, 'end' => $end));

	  	return $query->result_array();
	}
}
?>