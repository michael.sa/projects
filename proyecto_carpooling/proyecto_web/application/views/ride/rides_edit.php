<html>
<head>
	<title>TicoRides</title>
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/bootstrap.min.css">
  	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/style.css">
  	<script src="<?php echo base_url();?>assets/js/script.js"></script>
	<meta name="viewport" content="width=device-width, initial-scale=1">
</head>
<body class="body">

	<div class="container">
		<div class="row">
		  	<div class="col-md-4"></div>
		  	<div class="col-md-4">
		  		<img src="http://images.financialexpress.com/2014/12/carpooling.jpg" 
		  		class="img-responsive" alt="Responsive image">
		  	</div>
		 	<div class="col-md-4"></div>
		</div>
	</div>
	<br>

	<div class="row">
  		<div class="col-md-3 col-md-offset-2">
			<ul class="nav nav-tabs">
			 	<li role="presentation"><a href="<?php echo base_url().'Principal/load_dashboard'?>" target="_self">Dashboard</a></li>
			  	<li role="presentation" class="active"><a href="<?php echo base_url().'Ride/load_ride'?>" target="_self">Rides</a></li>
			  	<li role="presentation"><a href="<?php echo base_url().'User/load_settings'?>" target="_self">Settings</a></li>
			</ul>
		</div>
	  	<div class="col-md-3 col-md-offset-3">
	  		<ul class="nav nav-tabs">
	  			<li role="presentation" class="dropdown">
				 <a class="dropdown-toggle" data-toggle="dropdown" href="#" role="button">
				    <label class="welcome">Welcome <?php echo $user_name;?></label>
				    <img class='img_user' src='<?php echo base_url().'/uploads/'.$img?>'>
				   	<span class="caret"></span>
				  </a>
				    <ul class="dropdown-menu">
						<a href="<?php echo base_url();?>User/Logout">Logout</a>
				    </ul>
	  			</li>
	  		</ul>
		 </div>
	</div>
	<br>
	<div class="container">
		<div class="row">
  			<div class="col-md-3 col-md-offset-2">
  				<label class="tlabel">Rides</label>
  				<ul>
					<li><a href="<?php echo base_url().'Principal/load_dashboard'?> " target="_self">Dashboard ></a></li>
					<li><a href="<?php echo base_url().'Ride/load_ride'?> " target="_self">Rides ></a></li>
					<li>Edit</li>
				</ul>
  			</div>
		</div>
	</div>


	<div class="container">
		<form id="edit" class="form-horizontal" method="post" action="<?php echo base_url().'Ride/AgregarRide'?>">
			<div class="row">
			  	<div class="col-md-6 col-md-offset-3">
			  		<?php foreach ($ride as $rides):?>
						<label>Ride Name</label>
						<input class="form-control" type="text" name="rideName" value="<?php echo $rides['ride_name'];?>">
						<label>Start From</label>
						<input type="text" id="start" class="form-control" name="start" value="<?php echo $rides['start'];?>"/>
						<label>End</label>
						<input type="text" id="end" class="form-control" name="end" value="<?php echo $rides['end'];?>"/> 
						<p><label>Description</label></p>
						<p><textarea name="description" class="form-control" rows="2"><?php echo $rides['description'];?></textarea></p>
				
			  	</div>
			</div>
			<hr class="hr"></hr>
			<div class="row">
				<div class="col-md-2 col-md-offset-3">
					<label>Wen</label>
  					<p><label>Departure</label></p>
					<p><input name="departure" type="time" step="300" value="<?php echo $rides['departure'];?>"></label></p>
					<p><label>Stimated Arrival</label></p>
					<p><input name="arrival" type="time" step="300" value="<?php echo $rides['arrival'];?>"></label></p>
				</div> 	

				<div class="col-md-2 col-md-offset-2">
	  				<p><label><?php echo isset($dias_error)?$dias_error:'Select Days'?></label></p>
					<p><input type="checkbox" name="dia[]" id="Monday" value="Monday">Monday</p>
					<p><input type="checkbox" name="dia[]" id="Tuesday" value="Tuesday">Tuesday</p>
					<p><input type="checkbox" name="dia[]" id="Wednesday" value="Wednesday">Wednesday</p>
					<p><input type="checkbox" name="dia[]" id="Thursday" value="Thursday">Thursday</p>
					<p><input type="checkbox" name="dia[]" id="Friday" value="Friday">Friday</p>
					<p><input type="checkbox" name="dia[]" id="Saturday" value="Saturday">Saturday</p>
					<p><input type="checkbox" name="dia[]" id="Sunday" value="Sunday">Sunday</p>
				</div>
			</div>	
			<div class="container">
				<div class="row">
					<div class="col-md-1 col-md-offset-3">
			 			<a href="<?php echo base_url().'Principal/load_dashboard'?> " target="_self">Cancel</a>
					</div>
					<div class="col-md-2 col-md-offset-3">
						<button name="edit_ride" type="submit" class="btn btn-success" 
							value="<?php echo $rides['id_ride'];?>">Edit and Save
						</button>
		  			</div>
	    		</div>
			</div>
			<?php
				echo "<script type='text/javascript'>"; 
				echo "diasSelect('$rides[dia]');"; 
				echo "</script>";
			?>
			<?php endforeach;?>
			<input type="hidden" name="url_edit" value="<?php echo $_SERVER["REQUEST_URI"];?>">
		</form>
	</div>
	<div id="map"></div>
	<script src = "http://code.jquery.com/jquery-1.10.1.min.js" type="text/javascript"></script>
 	<script src="<?php echo base_url();?>assets/js/bootstrap.min.js"></script>
 	<script src="<?php echo base_url();?>assets/js/map.js"></script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBpigFKAB0g04kGberX6Lr22a0BRhMfsHI&libraries=places&callback=initMap"></script>
	
</body>
</html>