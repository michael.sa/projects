<html>
<head>
	<title>TicoRides.com</title>
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/bootstrap.min.css">
  <link rel="stylesheet" href="<?php echo base_url();?>assets/css/style.css">
	<meta name="viewport" content="width=device-width, initial-scale=1">
</head>
<body class="body">
	<div class="container">
    <form class="form-signin" action="<?php echo base_url().'User/Autentificar'?>" method="post">
      <img src="http://images.financialexpress.com/2014/12/carpooling.jpg" class="img-responsive" alt="Responsive image">
      <h3 class="form-signin-heading">Login</h3>
      <?php echo $error;?>
      <p class="error"><?php echo isset($login_error)?$login_error:'';?></p>
      <p><label>User Name</label></p>
      <p><input type="text" name="user_name" placeholder="User Name" class="form-control" required></input></p>
      <p><label>Password</label></p>
      <p><input type="password" name="password" placeholder="Password" class="form-control" required></input></p>
      <p><label>Not an User? </label><a href="<?php echo base_url().'User/load_registro'?>"> Register Here</a></p>
      <p><input class="btn btn-lg btn-primary btn-block" type="submit" 
      name="boton" value="Login"/></p>
      <p><a href="<?php echo base_url().'User/load_registro'?>" target="_self"> <input class="btn btn-lg btn-info btn-block" type="button" 
      name="boton" value="Sign In"/></a></p>
    </form>
  </div>

  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
 <script src="<?php echo base_url();?>assets/js/bootstrap.min.js"></script>
</body>
</html>