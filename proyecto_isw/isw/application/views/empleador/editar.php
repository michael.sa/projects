<!DOCTYPE html>
<html>
<head>
	<title>EditarPerfil</title>
  	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/bootstrap.min.css">
  	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/style.css">
	<meta name="viewport" content="width=device-width, initial-scale=1">
</head>
<body>
	    <div class="container">
        <div class="row">
            <div class="col-md-4"></div>
            <div class="col-md-4"> <img class="img-responsive" src="http://media.junkmail.co.ke/files/instance-01/main_photo/2014/07/28/46/45/7e06726066e769c443b53224537ae4c7-360x225.jpg"></div>
            <div class="col-md-4"></div>
          </div>
    </div>

     <div class="container">
        <div class="row">
            <div class="col-md-4"></div>
            <div class="col-md-4"></div>
            <div class="col-md-4"> 
              <div class="btn-group">
                <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" 
                  aria-haspopup="true" aria-expanded="false">
                  <?php echo $nombre;?><span class="caret"></span>
                </button>
                <ul class="dropdown-menu">
                  <li><a href="<?php echo base_url().'Empleador/load_editor'; ?>">Editar Perfil</a></li>
                  <li role="separator" class="divider"></li>
                  <li>
                  <a href="<?php echo base_url().'Empleador/Salir'?>">Logout</a></li>
                </ul>
              </div> 
            </div>
          </div>
      </div>

    <div class="container">
          <div class="row">
          <div class="col-md-4"></div> 
          <div class="col-md-4"><label><?php echo $msj?></label></div>
          <div class="col-md-4"></div>
        </div>
    </div>
	<div class="container">
	    <form class="form-signin" action="<?php echo base_url().'Empleador/EditarDatos'?>" method="post" autocomplete=false>
			<?php foreach ($empleador as $empleadores):?>
			<p><label>Nombre</label></p>
			<p><input type="text" class="form-control" name="nombre" value="<?php echo $empleadores['nombre'];?>" required></p>
			<p><label>Primer Apellido</label></p>
			<p><input type="text" class="form-control" name="p_apellido" value="<?php echo $empleadores['primer_apellido']; ?>" required></p>
			<p><label>Segundo Apellido</label></p>
			<p><input type="text" class="form-control" name="s_apellido" value="<?php echo $empleadores['segundo_apellido'];?>" required></p>
			<p><label>Email</label></p>
			<p><input type="email" class="form-control" name="email" value="<?php echo $empleadores['email'];?>" required></p>
			<p><label>Contraseña</label></p>
			<p><input type="password" class="form-control" name="password" placeholder="Contraseña" required></p>
			<p><label>Repetir Contraseña</label></p>
			<p><input type="password" class="form-control" name="r_password" placeholder="Verificar Contraseña" required></p>
    		<?php endforeach; ?>
			<p><input type="submit" class="btn btn-lg btn-primary btn-block" name="boton" value="Guardar Cambios"/></p>
		</form>
	</div>

  <div class="container">
      <div class="row">
      <div class="col-md-4"></div>
      <div class="col-md-3"></div>
      <div class="col-md-4">
        <p><a href="<?php echo base_url().'Principal/load_principal'?>" target="_self"> 
        <input class="btn btn-success" type="button" 
        name="boton" value="Volver"/></a></p>
      </div>
    </div>
    </div>
</body>
</html>