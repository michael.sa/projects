<!DOCTYPE html>
<html>
<head>
	<title>Registro</title>
  	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/bootstrap.min.css">
  	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/style.css">
  	
	<meta name="viewport" content="width=device-width, initial-scale=1">
</head>
<body>
	<div class="container">
        <div class="row">
            <div class="col-md-4"></div>
            <div class="col-md-4"> <img class="img-responsive" src="http://media.junkmail.co.ke/files/instance-01/main_photo/2014/07/28/46/45/7e06726066e769c443b53224537ae4c7-360x225.jpg"></div>
            <div class="col-md-4"></div>
        </div>
    </div>
    <div class="container">
    	<div class="row">
		  <div class="col-md-4"></div> 
		  <div class="col-md-4"><label><?php echo $msj?></label></div>
		  <div class="col-md-4"></div>
		</div>
    </div>
	<div class="container">
	    <form class="form-signin" action="<?php echo base_url().'Empleador/RegistrarEmpleador'?>" method="post" autocomplete=false>
			<p><label>Nombre</label></p>
			<p><input type="text" class="form-control" name="nombre" placeholder="Nombre" required></p>
			<p><label>Primer Apellido</label></p>
			<p><input type="text" class="form-control" name="p_apellido" placeholder="Primer Apellido" required></p>
			<p><label>Segundo Apellido</label></p>
			<p><input type="text" class="form-control" name="s_apellido" placeholder="Segundo Apellido" required></p>
			<p><label>Email</label></p>
			<p><input type="email" class="form-control" name="email" placeholder="Email" required></p>
			<p><label>Contraseña</label></p>
			<p><input type="password" class="form-control" name="password" placeholder="Contraseña" required></p>
			<p><label>Repetir Contraseña</label></p>
			<p><input type="password" class="form-control" name="r_password" placeholder="Verificar Contraseña" required></p>
			<p><input type="submit" class="btn btn-lg btn-primary btn-block" name="boton" value="Registrarme"/></p>
		</form>
	</div>
</body>
</html>