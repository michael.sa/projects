<!DOCTYPE html>
<html>
<head>
	<title>Publicar</title>
	<link rel="stylesheet" href="<?php echo base_url();?>assets/css/bootstrap.min.css">
  	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/style.css">
	<meta name="viewport" content="width=device-width, initial-scale=1">
</head>
<body>
	<div class="container">
        <div class="row">
            <div class="col-md-4"></div>
            <div class="col-md-4"> <img class="img-responsive" src="http://media.junkmail.co.ke/files/instance-01/main_photo/2014/07/28/46/45/7e06726066e769c443b53224537ae4c7-360x225.jpg"></div>
            <div class="col-md-4"></div>
          </div>
    </div>

     <div class="container">
        <div class="row">
            <div class="col-md-4"></div>
            <div class="col-md-4"></div>
            <div class="col-md-4"> 
              <div class="btn-group">
                <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" 
                  aria-haspopup="true" aria-expanded="false">
                  <?php echo $nombre;?><span class="caret"></span>
                </button>
                <ul class="dropdown-menu">
                  <li><a href="<?php echo base_url().'Empleador/load_editor'?>">Editar Perfil</a></li>
                  <li role="separator" class="divider"></li>
                  <li>
                  <a href="<?php echo base_url().'Empleador/Salir'?>">Logout</a></li>
                </ul>
              </div> 
            </div>
          </div>
      </div>

    
	<div class="container">
        <div class="row">
            <div class="col-md-4"></div>
            <div class="col-md-4">
              <div class="panel panel-default">
                <div class="panel-heading">Datos del Trabajador</div>
                <div class="panel-body"> 
      	           <?php foreach ($datos as $dato):?>
                    <p><img class='img-dato' src='<?php echo base_url().'/uploads/'.$dato['imagen']?>'></p>
        				    <p><label>Nombre Completo</label></p>
        						<p><label><?php echo $dato['nombre_comp'];?></label></p>
        						<p><label>Edad</label></p>
        						<p><label><?php echo $dato['edad'];?></label></p>
        						<p><label>Telefono</label></p>
        						<p><label><?php echo $dato['telefono'];?></label></p>
        						<p><label>Localidad</label></p>
        						<p><label><?php echo $dato['localidad'];?></label></p>
        						<p><label>Disponibilidad</label></p>
        						<p><label><?php echo $dato['disponibilidad'];?></label></p>
        					<?php  endforeach;?>
      					   <p><label>Habilidades</label></p>
      					   <?php foreach ($habilidades as $habilidad):?>
      						  <p><label><?php echo $habilidad['habilidad'];?></label></p>
      		    		<?php  endforeach;?>
                </div>
              </div>
            </div>
            <div class="col-md-4"></div>
        </div>
    </div>

    <div class="container">
    	<div class="row">
  		<div class="col-md-4"></div>
  		<div class="col-md-3"></div>
  		<div class="col-md-4">
        <p><a href="<?php echo base_url().'Principal/load_principal'?>" target="_self"> 
        <input class="btn btn-success" type="button" 
        name="boton" value="Volver"/></a></p>
  		</div>
		</div>
    </div>
</body>
</html>