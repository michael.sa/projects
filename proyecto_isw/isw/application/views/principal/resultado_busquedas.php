<html>
<head>  
	<title>BestBaBySitter.com</title>
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/bootstrap.min.css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/style.css">
	<meta name="viewport" content="width=device-width, initial-scale=1">
</head>
<body>
    <div class="container">
        <div class="row">
            <div class="col-md-4"></div>
            <div class="col-md-4"> <img class="img-responsive" src="http://media.junkmail.co.ke/files/instance-01/main_photo/2014/07/28/46/45/7e06726066e769c443b53224537ae4c7-360x225.jpg"></div>
            <div class="col-md-4"></div>
          </div>
    </div>

        <div class="container">
        <div class="row">
            <div class="col-md-4"></div>
            <div class="col-md-4"></div>
            <div class="col-md-4"> 
              <div class="btn-group">
                <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" 
                  aria-haspopup="true" aria-expanded="false">
                  <?php echo $nombre?><span class="caret"></span>
                </button>
                <ul class="dropdown-menu">
                  <li><a href="<?php echo base_url().'Empleador/load_editor'?>">Editar Perfil</a></li>
                  <li role="separator" class="divider"></li>
                  <li><a href="<?php echo base_url().'Empleador/Salir'?>">Logout</a></li>
                </ul>
              </div> 
            </div>
          </div>
      </div>
    <div class="container">
        <div class="row">
          <div class="col-md-4"></div>
          <div class="col-md-4"><h3>Resultados de busqueda para "<?php echo $lugar;?>"</h3></div>
          <div class="col-md-4"></div>
      </div>
    </div>
    <div class="container">
       <?php foreach ($trabajadores as $trabajador):?>
        <div class="row">
            <div class="col-md-4"></div>
            <div class="col-md-4">
              <div class="panel panel-default">
                <div class="panel-heading">
                <?php echo $trabajador['nombre_comp'];?>
                  <form method="post" action="<?php echo base_url().'Principal/load_datos'?> ">
                  <button type="submit" name="ver" class="btn btn-info btn-mas" 
                  value="<?php echo $trabajador['id_trabajador'];?>">Ver más</button>
                  </form>
                </div>
                <div class="panel-body"> 
                    <p><img class='img-principal' src='<?php echo base_url().'/uploads/'.$trabajador['imagen']?>'></p>
                    <p><label>Localidad <?php echo $trabajador['localidad'];?></label></p>
                    <P><label>Disponibilidad <?php echo $trabajador['disponibilidad'];?></label></P>
                    <p><label>Edad <?php echo $trabajador['edad'];?></label></P>
                </div>
              </div>
            </div>
            <div class="col-md-4"></div>
        </div>
        <?php endforeach;?>
    </div>
    
     <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
</body>
</html>
