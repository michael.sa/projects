<!DOCTYPE html>
<html>
<head>
	<title>Publicar</title>
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/bootstrap.min.css">
  	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/style.css">
  	<link rel="stylesheet" type="text/css" href="style.css">
	<meta name="viewport" content="width=device-width, initial-scale=1">
</head>
<body>
	<div class="container">
        <div class="row">
            <div class="col-md-4"></div>
            <div class="col-md-4"> <img class="img-responsive" src="http://media.junkmail.co.ke/files/instance-01/main_photo/2014/07/28/46/45/7e06726066e769c443b53224537ae4c7-360x225.jpg"></div>
            <div class="col-md-4"></div>
         </div>
    </div>
	<div class="container">
		<form class="form-signin" action="<?php echo base_url().'Trabajador/PublicarDatos'?>" method="post" enctype="multipart/form-data" autocomplete=false>
			<p><label>Favor seleccione una imagen para su perfil</label></p>
			<label for="fileToUpload">
				<img id ='img' width="200" height="200" src="https://image.freepik.com/free-icon/add-people-interface-symbol-of-black-person-close-up-with-plus-sign-in-small-circle_318-56589.jpg">
			</label>
	   		<input  class="input-img" type="file" name="userfile" id="fileToUpload" 
	    	onchange="document.getElementById('img').src = window.URL.createObjectURL(this.files[0])" required>
			<p><label>Nombre Completo</label></p>
			<p><input type="text" class="form-control" name="nombre" placeholder="Nombre" required></p>
			<p><label>Edad</label></p>
			<p><input type="text" class="form-control" name="edad" placeholder="Edad" required></p>
			<p><label>Telefono</label></p>
			<p><input type="tel" class="form-control" name="tel" placeholder="xx-xx-xx-xx" required></p>
			<p><label>Localidad</label></p>
			<p><input type="text" class="form-control" name="localidad" placeholder="Localidad" required></p>
			<p><label>Disponibilidad</label></p>
			<p><input type="text" class="form-control" name="disponibilidad" placeholder="Disponibilidad" required></p>
			<p><label>Favor seleccione sus Habilidades</label></p>
			<?php foreach ($habilidad as $habilidades):
			 	echo "<p><input type= 'checkbox'name = 'habilidades[]' value='".$habilidades['id_habilidad']."'>"
			 	.$habilidades['habilidad']."</p>";?>
			 <?php endforeach;?>
			<input type="submit" class="btn btn-lg btn-primary btn-block" name="boton" value="Publicar" />
		</form>
	</div>
</body>
</html>