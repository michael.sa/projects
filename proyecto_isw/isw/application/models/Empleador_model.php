<?php  
/**
* 
*/
class Empleador_model extends CI_Model
{
	
	function __construct()
	{
		
		parent::__construct();
	}

	//login
	function Autentificar($email, $contrasena) {
    $query = $this->db->get_where('empleador',
      array('email' => $email, 'contrasena' => $contrasena));

	  return $query->result_array();
  }

  //registro
   public function RegistrarEmpleador($data)
    {
       $this->db->insert('empleador', $data);
    }

//editar
	function CargarDatos($id) {
    $query = $this->db->get_where('empleador',
     array('id_empleador' => $id));
	  return $query->result_array();
  }

  function EditarDatos($id,$datos)
  {
	$query = $this->db->where('id_empleador', $id);
	$query = $this->db->update('empleador', $datos);
  }
}
?>