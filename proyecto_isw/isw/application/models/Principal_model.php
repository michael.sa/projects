<?php  
 /**
 * 
 */
 class Principal_model extends CI_Model
 {
 	
 	function __construct()
 	{
 		parent::__construct();
 	}

 	//pag_principal
 	public function CargarRandomTrabajadores()
 	{
 		$query = $this->db->query("SELECT * FROM trabajador ORDER BY rand() LIMIT 3");

 		return $query->result_array();
 	}


 	//mostrar_Datos
 	function CargarDatos($id)
	{
		$query = $this->db->get_where('trabajador',
     	array('id_trabajador' => $id));
	  	return $query->result_array();
	}

	function CargarHabilidades($id)
	{
		
		$query = $this->db->query("SELECT habilidad.habilidad FROM habilidad 
			INNER JOIN trabajador 
			INNER JOIN habilidad_trabajador 
			ON trabajador.id_trabajador = habilidad_trabajador.id_trabajador 
			AND habilidad.id_habilidad = habilidad_trabajador.id_habilidad AND trabajador.id_trabajador = $id");
			return $query->result_array();
	}

	//buscar
	function BuscarPorLugar($localidad)
	{

		$query = $this->db->get_where('trabajador',
     	array('localidad' => $localidad));
	  	return $query->result_array();
	}


 }

?>