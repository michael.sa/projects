<?php  
/**
* 
*/
class Trabajador_model extends CI_Model
{
	
	function __construct()
	{
		parent::__construct();
	}

	function PublicarTrabajador($data)
    {
       $this->db->insert('trabajador', $data);

    }

    function CargarHabilidades()
    {
    	$query = $this->db->get('habilidad');
    	return $query->result_array();
    }

    function ObtenerId()
    {
    	$id = $this->db->insert_id();
    	return $id;
    }

    function RegistrarHabilidades($data)
    {
		$query = $this->db->insert('habilidad_trabajador',$data);
    }
}
?>