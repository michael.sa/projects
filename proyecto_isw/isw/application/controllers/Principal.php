<?php  
/**
* 
*/
class Principal extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
	}

	 public function load_principal()
    {	
    	if (isset($_SESSION['loggedin']) && $_SESSION['loggedin'] == true) {
  		$data['nombre'] = $_SESSION['username'];
      $data['trabajadores'] = $this->Principal_model->CargarRandomTrabajadores();
      $this->load->view('principal/principal',$data);
		} else {
       $this->session->set_flashdata('error','Primero se debe logear');
        redirect('Empleador/load_login');
  			
		}

    
    }

    //Mostrar Datos
    public function load_datos()
  { 
    if (isset($_SESSION['loggedin']) && $_SESSION['loggedin'] == true) {
      $data['nombre'] = $_SESSION['username'];
        $id = $_POST['ver'];
  
    $data['datos'] = $this->Principal_model->CargarDatos($id);
    $data['habilidades'] = $this->Principal_model->CargarHabilidades($id);
    $this->load->view('principal/datos',$data);
    } else {
         $this->session->set_flashdata('error','Primero se debe logear');
        redirect('Empleador/load_login');
        
    }
    
  
  }

  function volver()
  {
    redirect('Principal/load_principal');
  }


}
?>