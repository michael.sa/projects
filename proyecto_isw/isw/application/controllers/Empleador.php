<?php  
/**
* 
*/
class Empleador extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
    $this->load->helper(array('form', 'url'));
    $this->load->library('form_validation');
	}

	public function load_login()
	{
    $data['msj'] = $this->session->flashdata('msj');
    $data['error'] = $this->session->flashdata('error');
 		$this->load->view('empleador/login',$data);
	}
 //login
	public function Autentificar()
    {
    $email = $_POST['email'];
		$password = $_POST['password'];
      
		$resultado = $this->Empleador_model->Autentificar($email, $password);
      	if (sizeof($resultado) > 0) {
      		foreach ($resultado as $resultados) {
      			$_SESSION['loggedin'] = true;
      			$_SESSION['id_empleador'] = $resultados['id_empleador'];
  				$_SESSION['username'] = $resultados['nombre'];
			
				    redirect('Principal/load_principal');
      		  }
        } else {
       	  $this->session->set_flashdata('error', 'Usuario o contraseña incorrectos');
			   redirect('Empleador/load_login');
       }
    }
//logout
    function Salir()
    {
    	session_destroy();
    	redirect('Empleador/load_login');

    }
    //Registro
    public function load_registro()
    {
       $data['msj'] = $this->session->flashdata('msj');
      $this->load->view('empleador/registros',$data);
    }
    //registro emp
	public function RegistrarEmpleador()
    {
     $this->form_validation->set_rules('password','r_password','matches');
     $this->form_validation->set_rules('email','valid_emails');
        $data = array(
          'nombre' => $_POST['nombre'],
    			'primer_apellido' => $_POST['p_apellido'],
    			'segundo_apellido' => $_POST['s_apellido'],
    			'email' => $_POST['email'],
    			'contrasena' => $_POST['password'],
    			
             );
         
        if ($this->form_validation->run()==true) {
          
        $this->Empleador_model->RegistrarEmpleador($data);
      
       redirect('empleador/load_login');
        } else {
           $this->session->set_flashdata('msj','Las contraseñas no coinciden');
          redirect('empleador/load_registro');
        }
    }

    //Editar Emp
    public function load_editor()
	{
		if (isset($_SESSION['loggedin']) && $_SESSION['loggedin'] == true) {
  	$data['nombre'] = $_SESSION['username'];
    $data['msj'] = $this->session->flashdata('msj');
    $data['empleador'] = $this->Empleador_model->CargarDatos($_SESSION['id_empleador']);
    $this->load->view('empleador/editar',$data);
		} else {
        $this->session->set_flashdata('error','Primero se debe logear');
  			redirect('Empleador/load_login');
  			
		}

	}
	//editar datos
	public function EditarDatos()
	{
     $this->form_validation->set_rules('password','r_password','matches');
     $this->form_validation->set_rules('email','valid_emails');
		$datos = array(
         	'nombre' => $_POST['nombre'],
			'primer_apellido' => $_POST['p_apellido'],
			'segundo_apellido' => $_POST['s_apellido'],
			'email' => $_POST['email'],
			'contrasena' => $_POST['password'],
			
         );

    if ($this->form_validation->run()==true) {
          
         $this->Empleador_model->EditarDatos($_SESSION['id_empleador'],$datos);
        redirect('Principal/load_principal');

        } else {
           $this->session->set_flashdata('msj','Las contraseñas no coinciden');
          redirect('Empleador/load_editor');
        }
	}

	//Buscar
	 public function load_buscar()
    {	
       	if (isset($_SESSION['loggedin']) && $_SESSION['loggedin'] == true) {
  		$data['nombre'] = $_SESSION['username'];
		} else {
  		 $this->session->set_flashdata('error','Primero se debe logear');
        redirect('Empleador/load_login');
		}
    	$lugar = $_POST['lugar'];
    	$data['lugar'] = $lugar;
    	$data['trabajadores'] = $this->Principal_model->BuscarPorLugar($lugar);
     	$this->load->view('principal/resultado_busquedas',$data);
    }

 	 //volcer a principal
    function Volver()
    {
    
    	$data['trabajadores'] = $this->Principal_model->BuscarPorLugar($lugarTemp);
     	$this->load->view('principal/resultado_busquedas',$data);
    }
}
?>