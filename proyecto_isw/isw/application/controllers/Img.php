<?php  
/**
* 
*/
class Img extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
	}

	 public function load_view()
    {	
      // $this->load->model('Login_model');
     // $data['dato'] = $this->Login_model->Cargar();
    	$this->load->view('img/imgs');
    }

     public function upload()
    { 
      $config = array(
    'upload_path' => "./uploads/",
    'allowed_types' => "gif|jpg|png|jpeg|pdf",
    'overwrite' => TRUE,
    'max_size' => "2048000", // Can be set to particular file size , here it is 2 MB(2048 Kb)
    'max_height' => "2000",
    'max_width' => "2000"
    );
    $this->load->library('upload', $config);
    if($this->upload->do_upload())
    {
    $data = array('upload_data' => $this->upload->data());
    $file_data = $this->upload->data(); 
    $data = $array = array(
      'nombre' => $_POST['nombre'],
      'imagen' => $file_data['file_name']
      );
  //  $this->load->model('Login_model');
   // $this->Login_model->GuardarImg($data);

    $data['upload_data'] =  $file_data;
    $data['img'] = base_url().'/uploads/'.$file_data['file_name'];
    $this->load->view('img/suss',$data);
    }
    else
    {
    $error = array('error' => $this->upload->display_errors());
    $this->load->view('img/imgs', $error);
    }
    }
}
?>