<?php  
/**
* 
*/
class Trabajador extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
	}

	 public function load_view()
    {
	
		$data['habilidad'] = $this->Trabajador_model->CargarHabilidades();  
        $this->load->view('Trabajador/publicaciones',$data);
    }


    function PublicarDatos()
    {
    	$config = array(
    	'upload_path' => "./uploads/",
    	'allowed_types' => "gif|jpg|png|jpeg|pdf",
    	'overwrite' => TRUE,
    	'max_size' => "2048000", 
    	'max_height' => "2000",
    	'max_width' => "2000"
    	);
    $this->load->library('upload', $config);
    if($this->upload->do_upload())
    {
	    $data = array('upload_data' => $this->upload->data());
	    $file_data = $this->upload->data(); 
	    $habilidades = $_POST['habilidades'];
	    $data = $array = array(
	    	'nombre_comp' => $_POST['nombre'],
			'edad' => $_POST['edad'],
			'localidad' => $_POST['localidad'],
			'disponibilidad' => $_POST['disponibilidad'],
			'telefono' => $_POST['tel'],
	      	'imagen' => $file_data['file_name']
	      );
	  	$this->Trabajador_model->PublicarTrabajador($data);

	    $ultimo_id = $this->Trabajador_model->ObtenerId();
		for($i=0; $i < count($habilidades); $i++){
			$array = array(
				'id_habilidad' => $habilidades[$i],
				'id_trabajador' => $ultimo_id
				 );
		$this->Trabajador_model->RegistrarHabilidades($array);	
		}

		redirect('Empleador/load_login');

    } else {
    $error = array('error' => $this->upload->display_errors());
    $this->load->view('Trabajador/publicaciones', $error);
    }
	}
    
}
?>